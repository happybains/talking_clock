package test.java;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.wisdomleaf.service.ClockService;
import com.wisdomleaf.service.impl.ClockServiceImpl;

class ClockTestCase {

	ClockServiceImpl clockService = new ClockServiceImpl();
	
	@Test
	public void testmiddayCondition() throws  IOException {  
	    assertEquals("Its midday",clockService.getTime("12:00"));  
	         }  


@Test
public void testmidnightCondition() throws  IOException {  
    assertEquals("Its midnight",clockService.getTime("00:00"));  
         }  

@Test
public void testonetoNinehr() throws  IOException {  
    assertEquals("its Six hrs and Fourty Five mins",clockService.getTime("06:45"));  
         }  

@Test
public void testeleventoNinteenhr() throws  IOException {  
    assertEquals("its Fourteen hrs and Fourty Five mins",clockService.getTime("14:45"));  
         }  

@Test
public void lastLimitTest() throws IOException {  
    assertEquals("its Twenty Three hrs and Fifty Nine mins",clockService.getTime("23:59"));  
         } 

}
