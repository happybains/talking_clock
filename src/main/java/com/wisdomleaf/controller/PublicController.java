package com.wisdomleaf.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.wisdomleaf.service.ClockService;
import com.wisdomleaf.util.JsonUtil;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/public")
public class PublicController {

    @Autowired
    private ClockService clockService;
   
    
    private static final Logger LOG = LoggerFactory.getLogger(PublicController.class);
    
    
    
	
    
	@ApiOperation(value = "Get time")
	@RequestMapping(value = "/getTime", method = RequestMethod.GET)
	public ResponseEntity<ObjectNode> generateTime(@RequestParam(name = "time", required = true) String time) {
			 try {
			String result =	clockService.getTime(time);
			ObjectNode rootNode = JsonUtil.getJsonObject(result);
	    	return new ResponseEntity<ObjectNode>(rootNode,HttpStatus.OK);
			} catch (Exception e) {
		    	ObjectNode rootNode = JsonUtil.getJsonObject("Invalid time");
		    	return new ResponseEntity<ObjectNode>(rootNode,HttpStatus.CONFLICT);
			} 
		
	}
	
	
	 
}
