package com.wisdomleaf.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.wisdomleaf.service.ClockService;
import com.wisdomleaf.util.CommonUtil;

@Service
public class ClockServiceImpl implements ClockService {

	private static final Logger LOG = LoggerFactory.getLogger(ClockServiceImpl.class);
	
	 public  String getTime(String time) 
	    { 

	    //	String time = "18:59";
	    	String result ;
	    	String timearry[]  = time.split("[:]", 0);
	    	CommonUtil.getHourPart(timearry[0]);
	    	CommonUtil.getHourPart(timearry[1]);
	    	if(time.equals("00:00")) {
	    		result = "Its midnight";
	    	} else if(time.equals("12:00")) {
	    		result= "Its midday";
	    	} else {
	    		result = "its "+CommonUtil.getHourPart(timearry[0])+" hrs and "+CommonUtil.getHourPart(timearry[1])+" mins";
	    	}
	    	 return result ;
	    }

	
	
}
