package com.wisdomleaf.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;



@Component
public class CommonUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(CommonUtil.class);
	
	public static String getHourPart(String string) {
		int hour = Integer.parseInt(string);
		String result = "Zero" ;
		if(hour >= 1 &&  hour <= 20) {
			result = getUnitPlace(string);
		} else if (hour >=20 && hour <= 29) {
			hour = hour%10 ;
			result = "Twenty "+getUnitPlace(Integer.toString(hour));
		} else if(hour >=30 && hour <= 39) {
			hour = hour%10 ;
			result = "Thirty "+getUnitPlace(Integer.toString(hour));
		}else if(hour >=40 && hour <= 49) {
			hour = hour%10 ;
			result = "Fourty "+getUnitPlace(Integer.toString(hour));
		}else if(hour >=50 && hour <= 59) {
			hour = hour%10 ;
			result = "Fifty "+getUnitPlace(Integer.toString(hour));
		}
		return result;
	}
	
	public static String getUnitPlace(String hour){
		  String result = ""; 
	    	switch(hour) {
	    	  case "01":
	    		  result = "One";
	    	    break;
	    	  case "02":
	    		  result = "Two";
	    	    break;
	    	  case "03":
	    		  result = "Three";
	    	    break;
	    	  case "04":
	    		  result = "Four";
	    	    break;
	    	  case "05":
	    		  result = "Five";
	    	    break;
	    	  case "06":
	    		  result = "Six";
	    	    break;
	    	  case "07":
	    		  result = "Seven";
	    	    break;
	    	  case "08":
	    		  result = "Eight";
	    	    break;
	    	  case "09":
	    		  result = "Nine";
	    	    break;
	    	  case "10":
	    		  result = "Ten";
	    	    break;
	    	  case "11":
	    		  result = "Eleven";
	    	    break;
	    	  case "12":
	    		  result = "Twelve";
	    	    break;
	    	  case "13":
	    		  result = "Thirteen";
	    	    break;
	    	  case "14":
	    		  result = "Fourteen";
	    	    break;
	    	  case "15":
	    		  result = "Fifteen";
	    	    break;
	    	  case "16":
	    		  result = "Sixteen";
	    	    break;
	    	  case "17":
	    		  result = "Seventeen";
	    	    break;
	    	  case "18":
	    		  result = "Eighteen";
	    	    break;
	    	  case "19":
	    		  result = "Nineteen";
	    	    break;
	    	  case "20":
	    		  result = "Twenty";
	    	    break;
	    	  case "0":
	    		  result = "Zero";
	    	    break;
	    	  case "1":
	    		  result = "One";
	    	    break;
	    	  case "2":
	    		  result = "Two";
	    	    break;
	    	  case "3":
	    		  result = "Three";
	    	    break;
	    	  case "4":
	    		  result = "Four";
	    	    break;
	    	  case "5":
	    		  result = "Five";
	    	    break;
	    	  case "6":
	    		  result = "Six";
	    	    break;
	    	  case "7":
	    		  result = "Seven";
	    	    break;
	    	  case "8":
	    		  result = "Eight";
	    	    break;
	    	  case "9":
	    		  result = "Nine";
	    	    break;
	    	    default:
	    	    result ="invalid time";
	    	}
	    	return result;
	    }


}
