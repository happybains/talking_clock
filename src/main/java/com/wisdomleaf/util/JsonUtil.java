package com.wisdomleaf.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class JsonUtil {

	public static ObjectNode getJsonObject(String message) {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode rootNode = mapper.createObjectNode();
		rootNode.put("message", message);
		return rootNode;
	}
	
	public static ObjectNode getJsonObjectWithStatus(String message,String status) {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode rootNode = mapper.createObjectNode();
		rootNode.put("message", message);
		rootNode.put("status", status);
		return rootNode;
	}
	
	public static ObjectNode getJsonObjectWithStatusAndImageUrl(String message,String status,String url) {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode rootNode = mapper.createObjectNode();
		rootNode.put("message", message);
		rootNode.put("status", status);
		rootNode.put("imageUrl", url);
		return rootNode;
	}
	
	public static ObjectNode getJsonObjectWithStatusUserUniqueId(String message,String status,String userUniqueId) {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode rootNode = mapper.createObjectNode();
		rootNode.put("message", message);
		rootNode.put("status", status);
		rootNode.put("userUniqueId", userUniqueId);
		return rootNode;
	}
	
	public static ObjectNode getJsonObjectWithStatusUserUniqueIdAndImageStatus(String message,String status,String userUniqueId,String imageUrl,String userName) {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode rootNode = mapper.createObjectNode();
		rootNode.put("message", message);
		rootNode.put("status", status);
		rootNode.put("userUniqueId", userUniqueId);
		rootNode.put("image", imageUrl);
		rootNode.put("userName", userName);
		return rootNode;
	}
	
	public static ObjectNode getJsonObjectWithClientSecretAndCustmoreId(String secretId,String custId) {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode rootNode = mapper.createObjectNode();
		rootNode.put("custmorId", custId);
		rootNode.put("clientSecretId", secretId);
		return rootNode;
	}
	
	public static ObjectNode getJsonObject(String key, String value) {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode rootNode = mapper.createObjectNode();
		rootNode.put(key, value);
		return rootNode;
	}
	
	public static ObjectNode getJsonObject(int errorCode, String message) {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode rootNode = mapper.createObjectNode();
		rootNode.put("errorCode", errorCode);
		rootNode.put("message", message);
		return rootNode;
	}
	
}
