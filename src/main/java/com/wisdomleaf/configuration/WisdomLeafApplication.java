package com.wisdomleaf.configuration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@ComponentScan(basePackages = {"com.wisdomleaf"})
@EnableAutoConfiguration
@EntityScan("com.wisdomleaf.model")
public class WisdomLeafApplication {

	public static void main(String[] args) {
		SpringApplication.run(WisdomLeafApplication.class, args);
	}

}
